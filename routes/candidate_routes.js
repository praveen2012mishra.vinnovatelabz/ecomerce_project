let express = require("express");
let router = express();
let bodyParser = require("body-parser");
//const cors = require("cors");
const cors = require("cors");
const dotenv = require("dotenv");
dotenv.config();
let productController = require("../controllers/product_controller");
router.use(cors());
let urlEncodedParser = bodyParser.urlencoded({ extended: true });
router.use(bodyParser.json());
//let display shop route means product will show
router.get("/searchProducts", (req, res) => {
  //res.sendFile(__dirname + "/" + "index.html");
  let value = JSON.stringify(productController.getProduct());
  //res.redirect("/showAddProductForm");
  return res.send(JSON.stringify(value));
});

//show add form
router.get("/showAddProduct", (req, res) => {
  //res.sendFile(__dirname + "/" + "index.html");
  res.send(JSON.stringify(productController.getProduct()));
  //return productController.getProduct(req, res);
});

router.get("/getCart", (req, res) => {
  //res.sendFile(__dirname + "/" + "index.html");
  //console.log(JSON.stringify(productController.getCartController()));
  
  res.send(JSON.stringify(productController.getCartController()));
  //return productController.getProduct(req, res);
});

//search product in shop.
router.post("/addCart", (req, res) => {
  res.set("Content-Type", "application/json");
  let id=req.body
  //console.log(req.body,'============>');
  productController.addCart(req, res, id)
});

//delete item
router.post('/deleteItem',(req,res)=>{
  res.set("Content-Type", "application/json");
  //console.log(req.body,"===========================>");
  productController.deleteItemCartController(req.body)
  res.redirect('/getCart')
})

//add product in shop
//const product=[{itemName:'Pen',itemType:'Stationary'}];
router.post("/addProduct", urlEncodedParser, (req, res) => {
  res.set("Content-Type", "application/json");
 
  let body = {
    itemId:req.body.itemId,
    itemName: req.body.itemName,
    itemType: req.body.itemType,
    itemImage:req.body.itemImage
  };
  //console.log(body,"------------------------>",req.body);
  productController.addProduct(req, res, body);
  res.redirect('/showAddProduct')
  //res.send({ status: 'SUCCESS' });
});

router.post('/Payment',urlEncodedParser,(req,res)=>{
  let body={
    token:req.body.token,
    amount:req.body.amount
  }

})

const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
//console.log(stripe);

const stripeChargeCallBack=res=>(stripeErr,stripeRes)=>{
 if(stripeErr){
  res.status(500).send({error:stripeErr})
 }
 else{
   res.status(200).send({success:stripeRes});
 }
}

router.post("/Payment",urlEncodedParser, (req, res) => {
  const body = {
    token: req.body.token,
    amount: req.body.amount
  };
  stripe.charges.create(body, stripeChargeCallback(res));
});
module.exports = router;
