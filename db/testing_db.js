let MongoClient = require("mongodb").MongoClient;
let url = "mongodb://localhost:27017";
let db_name, collectionName;

MongoClient.connect(url, (err, db) => {
  db_name = db.db("db1");
  let intro = [
    {
      name: "Praveen",
      last_name: "Mishra",
    },
    {
      name: "Praveen",
      last_name: "Mishra",
    },
    {
      name: "Praveen",
      last_name: "Mishra",
    },
  ];
  collectionName = db_name.collection("intro");
  collectionName.insertMany(intro, (err, res) => {
    //console.log(res,'===============================>');
  });
  collectionName.find({ last_name: "Mishra" }, (err, res) => {
    //console.log(res, "===============================>");
  });
  collectionName.aggregate([ {$group: { _id: {REF: "$REF"} } } ],(err,res)=>{
    console.log(res, "===============================>");
  })
});
