const express = require('express');
const app = express();
const dotenv = require("dotenv");
dotenv.config()
//This is use for compression of the file
const compression = require('compression');
const productRoutes=require('./routes/candidate_routes');
const connectDB=require('./db/Connection');
//This is use for logging the request, response, error in app.
const morgan = require('morgan');
//Helmet will add http header and give security.
const helmet = require('helmet')

app.use(compression());
app.use(productRoutes);
app.use(helmet())

app.use(morgan('combined',(req,res)=>{
    console.log(req,'==================>');
    
}));
//app.listen(process.env.PORT);