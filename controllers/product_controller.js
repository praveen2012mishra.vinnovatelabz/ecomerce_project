let Product= require('../models/product_models');

exports.getProduct=(req,res)=>{
   const product=Product.showProduct();
   return product;
}

exports.addProduct=(req,res,body)=>{
   const product=new Product(JSON.stringify(body));
   product.addProduct();
}

exports.addCart=(req,res,idArray)=>{
   const product=new Product();
   product.addCart(idArray);
}

exports.getCartController=(req,res)=>{
   const product=Product.showCart();
   return product;
}

exports.deleteItemCartController=(id)=>{
   const product=new Product();
   product.deleteModel(id)
}